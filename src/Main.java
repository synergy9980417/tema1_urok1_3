import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }

//
//        Урок 3. Переменные. Оперативная память. Дебаг.
//                Цель задания:
//        Научится создавать решения с переменными. Научится работать
//        с готовыми решениями.
//                Задания:
//        1.
//        Где хранятся переменные? Сколько переменных можно задать в
//        программе? Чем ограничен размер?


//        В Java переменные хранятся в памяти компьютера. Количество переменных, которые можно задать в программе, зависит от доступной оперативной памяти и ограничений, установленных виртуальной машиной Java (JVM). Общее количество переменных не является фиксированным числом и может меняться в зависимости от ресурсов системы и потребностей программы.
//
//                Размер переменных ограничен их типом данных. Например, целочисленные переменные типа int занимают 4 байта памяти, переменные типа long - 8 байт, переменные типа double - 8 байт и т.д. Таким образом, размер переменных определяется типом данных, который вы используете для их объявления.


//        2.
//        Пользователь вводит строку, выведите ее длину
            Integer l1 = ScannerStr.inputStrForGetLen(-1);
        System.out.println("!!!!!!!!!!!!!!!!!!!!!3!!!!!!!!!!!!!!!!!!!!");
            //        3.
//        Пользователь вводит две строки, выведите сумму их длин
            Integer sumL = 0;
            for (int i = 0; i < 2; i++) {
                sumL += ScannerStr.inputStrForGetLen(i);
            }
                System.out.println("сумма длин введёных строк = " + sumL);
        System.out.println("!!!!!!!!!!!!!!!!!!!!!4!!!!!!!!!!!!!!!!!!!!");

            //        4.
//        Пользователь вводит три строки. Найти, какая из них короче всех.
        Integer minL=0, indexMinL=0;
        for (int i = 0; i < 3; i++) {
            l1 = ScannerStr.inputStrForGetLen(i);
            if (minL < l1) {minL=l1;indexMinL=i;}
        }
        System.out.println("самая длинная строка из введённых трёх была №: " + (indexMinL+1));
        System.out.println("!!!!!!!!!!!!!!!!!!!!!5!!!!!!!!!!!!!!!!!!!!");


//        5.
//        Пользователь вводит три дробных числа. Вывести те из них,
//        квадратный корень которых меньше
//        2.
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            Float f1=0f;
            System.out.println("введите число №"+(i+1));
            f1=scanner.nextFloat();
            if (Math.sqrt(f1)<2){
                System.out.println("у этого числа: ("+f1+") корень меньше 2");
            } else {
                System.out.println("число не удовлетворяет условию, корень больше 2");
            }

        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!8!!!!!!!!!!!!!!!!!!!!");
//        6.
//        Пройдите в дебаге программу:
//        int x = 1;
//        while (x>=
//                -
//                        3) {
//            System.out.print(x);
//            x = x
//                    -
//                    1;
//        }
//        Сколько раз выводится икс?

//       Сделано. ответ 3

//        7.
//        Пройдите в дебаге программу:
//        String str = “Hell”;
//        while (str.length()<10) {
//            str = str + “o”;
//        }
//        Сколько раз вызывается метод str.leng
//        th() ?

//        Ответ 7

//                8.
//        Пользователь вводит два числа. Разделить меньшее на большее и
//        вывести результат.


        System.out.println("введите первое число:");
        Double d1=scanner.nextDouble();
        System.out.println("введите второе число:");
        Double d2=scanner.nextDouble();
        if (d1>d2){
            System.out.println(d2+"/"+d1+"="+(d2/d1));
        } else if (d2>d1) {
            System.out.println(d1+"/"+d2+"="+(d1/d2));
        } else {
            System.out.println("числа равны, ничего не делаем");
        }

        System.out.println("!!!!!!!!!!!!!!!!!!!!!9!!!!!!!!!!!!!!!!!!!!");
//        9.
//        Пользователь вводит строку. Используя substring, вывести первые
//        5 символов.



        System.out.println("введите любую строку, в которой выведется только 5 первых символов:");
        System.out.println(scanner.nextLine().substring(0,5));

        System.out.println("!!!!!!!!!!!!!!!!!!!!!10!!!!!!!!!!!!!!!!!!!!");
//        10.
//        Считайте boolean
//        -
//                переменную. Если пользователь ввел true,
//                вывести “истина”, иначе ничего не
//        стоит выводить.

        System.out.println("введите true или false");
        if (scanner.nextBoolean()==true){
            System.out.println("Истина");
        };

        System.out.println("!!!!!!!!!!!!!!!!!!!!!11!!!!!!!!!!!!!!!!!!!!");
//        11.
//        Пользователь вводит 3 числа. Сделайте три boolean переменных:
//        есть ли среди введённых четное, есть ли среди введённых отрицательное, есть
//        ли число больше тысячи

        int i1;
        Boolean b4et=false,bOtr=false,bBig=false;
        for (int i=0;i<3;i++){
            System.out.println("введите "+i+" число:");
            i1=scanner.nextInt();
            if (i1%2==0){b4et=true;}
            if(i1<0){bOtr=true;}
            if (i1>1000){bBig=true;}
        }
        System.out.println("b4et="+b4et+";bOtr="+bOtr+";bBig="+bBig);

        System.out.println("!!!!!!!!!!!!!!!!!!!!!12!!!!!!!!!!!!!!!!!!!!");
//        12.
//        Пользователь вводит три строки, используя .substring(0, x)
//        выведите эти строки
//                , обрезав те, что длиннее самой короткой. Пример ввода:
//«повар», «поделка», «лампочка». Вывод: «повар», «подел», «лампо»

        System.out.println("введите первую строку");
        String str1=scanner.nextLine();
        int min=str1.length();

        System.out.println("введите вторую строку");
        String str2=scanner.nextLine();
        if (str2.length()<min){min=str2.length();}

        System.out.println("введите третью строку");
        String str3=scanner.nextLine();
        if (str3.length()<min){min=str3.length();}

        System.out.println(str1.substring(0,min));
        System.out.println(str2.substring(0,min));
        System.out.println(str3.substring(0,min));


        System.out.println("!!!!!!!!!!!!!!!!!!!!!13!!!!!!!!!!!!!!!!!!!!");
//        13.
//        Мини
//                -
//                игра в слова. Первый игрок вводит слово. Потом второй
//        игрок вводит два числа, с какого по какой символ можно найти слово внутри
//        исходного, используя substring. Потом первый игрок вводит два числа.
//        Побеждает тот, чье слово длиннее. Пример:
//        1: революцио
//                нный
//        2: 0 3 (вывод: рев)
//        1: 7 13 (вывод: ионный)
//        Победил игрок 1



        System.out.println("введите строку");
        str1=scanner.nextLine();
        min=str1.length();
        int maxL=0;

        byte sub1=0,sub2=0;
        System.out.println("Первый игрок введите первое число от 0 до "+min);
        sub1=scanner.nextByte();
        System.out.println("введите второе число от 0 до "+min);
        sub2=scanner.nextByte();
        System.out.println("вы нашли вот это слово "+str1.substring(sub1,sub2)+" в этой сроке "+str1+". Бог вам судья." );
        maxL=str1.substring(sub1,sub2).length();

        System.out.println("Второй игрок введите первое число от 0 до "+min);
        sub1=scanner.nextByte();
        System.out.println("введите второе число от 0 до "+min);
        sub2=scanner.nextByte();
        System.out.println("вы нашли вот это слово "+str1.substring(sub1,sub2)+" в этой сроке "+str1+". Бог вам судья." );
        if (maxL>str1.substring(sub1,sub2).length()){
            System.out.println("победил первый");
        }
        else if (maxL<str1.substring(sub1,sub2).length()){
            System.out.println("победил второй");
        } else{
            System.out.println("ничья");
        }




//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода
//        -
//                вывода данных
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов




    }
}